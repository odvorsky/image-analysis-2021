%% clean all
clc, clear, close all;
%% load and show image
RGB = imread('pillsetc.png');
I = rgb2gray(RGB);
bw = imbinarize(I);
imshow(bw);
%%

octagon = strel('octagon', 6);

disk = strel('disk', 5);

imshow(imerode(bw, octagon));
%% 
a = imread('text.png');
mask = false(size(a));
mask(1:50, 1:50) = 1;
result = imreconstruct(mask, a);
imshow(result)

%%
a = imread('tire.tif');
cropped = imcrop(a);
%%
se = strel('disk', 8);
binary = cropped > 80;
%%
dil = imdilate(binary, se);
dil = imdilate(dil, strel('disk', 5));
dil = imerode(dil, strel('disk', 7));
dil = imerode(dil, strel('disk', 3));
%%
imshow(dil);
%%
dist = bwdist(dil);
val = max(dist(:));
imshow(dist, []);
%% 
a = imread('cell.tif');
e = edge(a);

se = strel('disk', 3);
dil = imdilate(e, se);
dil = imerode(dil, se);
dil = imdilate(dil, strel('disk', 6));
dil = imerode(dil, strel('disk', 6));

%%
dil = bwperim(dil);
%%
imshow(imoverlay(a, dil));
%% 
imshow('coins.png')

%% skeletonize
img = imread('circles.png');
imshow(bwdist(~img), []);

%%
img = imread('circles.png');
iter = 6;
nhood = [0 1 0; 0 1 0 ; 0 1 0];  
nhoodh = [0 0 0; 1 1 1 ; 0 0 0]; 
result = img;
for i = 1:iter
    result = imerode(result, nhood);
    result = imerode(result, nhoodh);
end
imshow(result);
%% 
img = imread('rice.png');
imshow(img);
%%
result = img - imopen(img, strel('disk', 8));
imshow(imadjust(result));

%%
moon = imread('moon.tif');
res = imbothat(moon, strel('disk', 3));
imshow(res, []);

%% 
o = imread('cameraman.tif');
d1 = imdilate(o, strel('disk', 3));
d2 = imdilate(o, offsetstrel('ball', 3, 3));
figure(1); imshow(d1);
figure(2); imshow(d2);
%% circles 
img = imread('circles.png');
img = bwdist(~img);
%% 
bw = imregionalmax(img);
imshow(bw, []);


