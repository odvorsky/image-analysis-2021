%% clean all
clc, clear, close all;
%%
%% Equalization using cumulative histogram
clear; clc
%%
A = imread('pout.tif');
[counts, bins] = histcounts(A);
cdf = cumsum(counts);
cdfmin = min(cdf);
npixels = numel(A);
nlevels = numel(bins);

hgram = round((cdf-cdfmin/npixels-cdfmin) * (nlevels - 1));
C = histeq(A,hgram);

imhist(C);
%%
sig = [1 1 1 2 2 2 3 3 12 4 3 3 3 3 3];
kernel = [1 1 1] / 3;

hx = linspace(0,1,3);
h = normpdf(hx,0,3);

result = conv(sig, kernel, 'same');
figure;
plot(sig);
figure;
plot(result);
%%
frog = imread('blackrainfrog.jpg');
frog = rgb2gray(frog);
kernel = ones(10) / 100;

result = conv2(frog, kernel, 'same');
imshow(result, []);

%% 
cman = imread('cameraman.tif');
kernel1 = [ -1 0 1 
            -1 0 1  
            -1 0 1 ];

res1 = conv2(cman, kernel1, 'same');
figure;
imshow(res1, []);
kernel2 = [ 1 1 1 
            0 0 0  
            -1 -1 -1 ];
        
res2 = conv2(cman, kernel2, 'same');
figure;
imshow(res2, []);
%%
imshow(C);
%%

zr = zeros(250, 250);
zr(100:200, 100:200) = 255;

kernel1 = [ -1 0 1 
            -1 0 1  
            -1 0 1 ];
        
result1 = conv2(zr, kernel1, 'same');

kernel2 = [ 1 1 1 
            0 0 0  
            -1 -1 -1 ];

kk = fspecial('laplacian');

kkk = fspecial('gaussian', 5);
        
result2 = conv2(zr, kernel2, 'same');

M = max(res, [], 'all');
[x, y] = find(res == M);
res(x,y) = 10000;
imshow(res,[]);
        
%%
o3=imread('zubr.jpeg');
o3 = rgb2gray(o3);
button=1;
x=0;y=0;
figure;ax = axes;
while button==1
    if x>100 && y >100
        z_ekv=o3;
        z_ekv(y-40:y+40,x-40:x+40)= histeq(z_ekv(y-40:y+40,x-40:x+40));
        imshow(z_ekv,'Parent',ax);
    end
    if x==0 && y==0
        imshow(o3);
    end
    [x,y, button]=ginput(1);
end

%%
fspecial
%% load and show image
RGB = imread('pillsetc.png');
I = rgb2gray(RGB);
bw = imbinarize(I);
%% apply sobel
sobel = fspecial('sobel');
resSobel = imfilter(bw, sobel);
imshow(resSobel);
%% prewitt
prewitt = fspecial('prewitt');
tPrewitt = prewitt';
resPrewitt = conv2(bw, prewitt);
resTPrewitt = conv2(bw, tPrewitt);
figure(1);
subplot 121; imshow(resPrewitt);
subplot 122; imshow(resTPrewitt);
%%
figure(2); imshow(resPrewitt+resTPrewitt, []);

%% edges...
imshow(edge(imread('circuit.tif'), 'Canny'));

%% 1D FT
%% 2x sinus
Fs = 1000;            % Sampling frequency                    
T = 1/Fs;             % Sampling period       
L = 1500;             % Length of signal
t = (0:L-1)*T;        % Time vector
S1 = 0.7*sin(2*pi*50*t)+ sin(2*pi*100*t); % 50hz s amplitudou 0.7 a 100hz s aplitudou 1
S1 = S1 +  randn(size(t));
subplot(211);
plot(S1);
%%
Y = fft(S1); % fourierovka
P2 = abs(Y/L); % normalizace aby nam dobre vychazela freq (ka�d� komponenta se vypo��t�va z cel�ho sign�lu)
P1 = P2(1:L/2+1); % ziskani amplitudy - sta�� vz�t jen p�lku hodnot, symetrie podle 0, sta�� n�m jen to kladn�
P1(2:end-1) = 2*P1(2:end-1); % pro korektni interpretaci amplitudy nasobim 2, kvuli symetrii pod�l 0 jsme toti� vyhodili pulku elementu s druhou polovinou hodnoty

f = Fs*(0:(L/2))/L;
subplot(212);
plot(f,P1); 

title('Single-Sided Amplitude Spectrum of S(t)');
xlabel('f (Hz)');
ylabel('|P1(f)|');
%% vlnity plech na zaklade repmat a jeho frekven�n� spektrum
close all
pocet_bodu = 256;
sin_plech = sin(linspace(1,50,pocet_bodu)); % vytvori sinusovy signal s poctem bodu si, linspace vytvori vektor s hranicemi 1 a 50 a poctem bodu si
A = repmat(sin_plech, pocet_bodu, 1); % vytvori matici, ktera vznikne spojenim sin_plech v pocet_bodu-krat
figure;
subplot(1, 2, 1);
imshow(A, []);
title('vlnity plech');
%%
% vypocet spektra
spektrum_obr = fft2(A); % vypocet spektra pomoci rychle fourierovy transformace pro 2D obraz
subplot(1, 2, 2)
spektrum_upraveno= log(fftshift(abs(spektrum_obr))); % posouva nulove frekvence do stredu
imshow(spektrum_upraveno, []) % pokud je�t� zlogaritmujeme, lepe se interpretuj� hodnoty bl�zko 0
title('spektrum')
% vysoke frekvence jsou na krajich, nulova ve stredu
% nizke frekvence reprezentuji nem�nn� ��sti obrazu, naopak ty vysok�
% reprezentuj� kontury, linie, hrany atd.
%% fft na fotce - pillsetc - filtrace n�zk�ch a vysok�ch frekvenc�
image_src = (imread('pillsetc.png'));
im_gr= rgb2gray(image_src);
% vypocet spektra sedotonoveho obrazku
spektrum_obr = fft2(im_gr);
spektrum_obr_uprava = log(fftshift(abs(spektrum_obr)));
subplot(3,3,1)
imshow(im_gr)
title('Puvodni obrazek v sedotonu')
subplot(3,3,2)
imshow(spektrum_obr_uprava, [])
title('spektrum puvodniho')
% filtrace
im_size = size(im_gr); %rozmery obrazu
d = 20; % sirka intervalu pro ctverec binarni masky
maska_dp = zeros(im_size(1), im_size(2)); % tvorba masky
maska_dp((im_size(1)/2)-d:(im_size(1)/2)+d, (im_size(2)/2)-d:(im_size(2)/2)+d) = 1; % vytvoreni masky ze stredu
% filtrace dolni propusti
spektrum_filt_dp = fftshift(spektrum_obr) .* maska_dp; % predpis pro filtraci ve frekvenci oblasti, spektrum je vynasobeno maskou
img_dp = ifft2(spektrum_filt_dp); %zpetna fourierova transformace
% vykresleni vysledku filtrace DP
subplot(3, 3, 4)
imshow((abs(img_dp)), []),title('po DP')
title('Obrazek po DP')
subplot(3,3,5)
imshow(log(abs(spektrum_filt_dp)), [])
title('Spektrum po DP')
subplot(3, 3, 6)
imshow(maska_dp)
title('Maska DP')
% pouziti inverzni masky, horni propust
spektrum_filt_hp = fftshift(spektrum_obr) .* (~maska_dp);
img_hp = ifft2(spektrum_filt_hp); %zpetna fourierova transformace
% vykresleni vysledku filtrace HP
subplot(3, 3, 7)
imshow((abs(img_hp)), []),title('po HP')
title('Obrazek po HP')
subplot(3,3,8)
imshow(log(abs(spektrum_filt_hp)), [])
title('Spektrum po HP')
subplot(3, 3, 9)
imshow(~maska_dp)
title('Maska HP')
%% PCA
clc, clear, close all;
nComp = 4;
A = imread('faces/admars.1.jpg');
Ag = double(rgb2gray(A));
mean = mean(Ag);
AgMean = Ag - mean;
[coeff, score] = pca(AgMean);
reconstruction = score(:,1:nComp)*coeff(:,1:nComp)';
reconstruction=uint8(reconstruction + mean);
imshow(reconstruction);
%%
clc, clear, close all;
D = 'faces';
S = dir(fullfile('faces','*.jpg')); 
for k = 1:numel(S)
    A = imread(D + "/" + S(k).name);
    Ag = rgb2gray(A);
    res = pca(double(Ag), 'NumComponents', 20, 'Economy',false);
    imshow(res, []);
end


