%% clean all
clc, clear, close all;
%% load and show image
A = imread('peppers.png');
imshow(A);
%% rgb -> gray
Ag = rgb2gray(A);
imshow(Ag);
%% rgb -> bw
bw = imbinarize(Ag);
imshow(bw)
%% rgb -> bw
bw = im2bw(A);
imshow(bw);
%% rgb -> ind
[bw, map] = rgb2ind(A, 32);
imshow(bw, map);
%% RGB disassemble
R=A(:,:,1);
G=A(:,:,2);
B=A(:,:,3);
%jako grayscale
figure;title('jednotlive slozky jako gray')
subplot(2,2,1);imshow(A)
subplot(2,2,2);imshow(R)
subplot(2,2,3);imshow(G)
subplot(2,2,4);imshow(B)
%% only single RGB channels in RGB matrix 
nuly=zeros(size(A,1),size(A,2));
R1=cat(3,R,nuly,nuly); %doplneni nulovymi maticemi
G1=cat(3,nuly,G,nuly);
B1=cat(3,nuly,nuly,Ag);

figure;title('jednotlive slozky barevne')
subplot(2,2,1);imshow(A)
subplot(2,2,2);imshow(R1)
subplot(2,2,3);imshow(G1)
subplot(2,2,4);imshow(B1)

%% own rgb2gray (NTSC convention)
R=A(:,:,1);
G=A(:,:,2);
B=A(:,:,3);
ownAg = 0.2989*R + 0.5870*G + 0.1140*B;
imshow(ownAg);
%% own binary
ownBw = Ag > 85;
imshow(ownBw);
%% own negativ
ownNeg = 255 - A;
imshow(ownNeg);
%% gray2bw a vlastni zpusob gray2bw
Ag=rgb2gray(A);
imshow(Ag,[])
 
imshow(Ag>128)
%% film zhoruj�c� se kvality obrazu (indexov� obraz)
a=0;
pocetBarev = [1 2 2 2 3 3 4 5 6 7 8 9 10 20 30 50 100 256];
%kolik barev je v dan�m kroku (barvy jsou zvoleny tak, aby to vypadalo rozumne)
figure;
for ii =pocetBarev
    [img2, mapa] = rgb2ind(A,ii);%Prevod RGB obrazu na indexov�
    imshow(img2,mapa);%v mape jsou k index?m prirazeny barvy
    %img2 obsahuje pouze indexy
    hold on;
    pause(.1)%nen� nutn� ps�t 0 
end
for ii =pocetBarev(end:-1:1)
    [img2, mapa] = rgb2ind(A,ii);
    imshow(img2,mapa);
    hold on;
    pause(.1)
end
%% film - rozedn�v�n� - men� se �roven prahov�n� v cyklu
%ukoncen�: ctrl + c
figure; ax = axes;%vytahnu osy kvuli prekreslovani obrazu

THR=0;
for i=1:3;%animace probehne 3x
while (THR<=255)
    imshow(Ag>THR,'Parent',ax);% Parent z duvodu prekresleni misto znovunakresleni
    THR=THR+10;
    pause(0.05)
end
while (THR>=0)
    imshow(Ag>THR,'Parent',ax)
    THR=THR-10;
    pause(0.05)
end
end
%% jednoduchy tvary
sizeX = 500;
sizeY = 500;
blue = zeros(sizeX, sizeY);
blue(225:275, 225:275) = 255;

red = zeros(sizeX, sizeY);
red(450:500, 1:50) = 255;

green = zeros(sizeX, sizeY);
green(450:500, 400:500) = 255;

green(blue == 255) = 255;
blue(blue == 255) = 0;
result = cat(3, red, green, blue);
imshow(result);
%% obarven� 
green(blue == 255) = 255;
blue(blue == 255) = 0;
result = cat(3, red, green, blue);
imshow(result);
%%
bw = imbinarize(rgb2gray(result));
dist = bwdist(1 - bw, 'euclidean');
maxN = max(dist);
imshow(dist, []);
%% prol�n�n� kan�l�
greens = imread('greens.jpg');
gR = greens(:,:,1);
gG = greens(:,:,2);
gB = greens(:,:,3);

combined = cat(gR, G, B);
imshow(combined);

%% prevest obrazek na double
% Prevede jakoukoliv matici do rozsahu 0-1.
% uint-8 obcas pretyka
% pouze na sedotonove obrazy
ju = mat2gray(Ag);
imshow(ju)

%% zmenseni obrazu
A_zmenseny = imresize(A,0.3);% 30 % puvodni velikosti
imshow(A_zmenseny,[])

%% zvetseni
A_zvetseny = imresize(A,[300 400]);% px - nezachova proporce
imshow(A_zvetseny)

%% vyrez kousku obrazu
Vyrez = A(50:100,170:250,:);
imshow(Vyrez)

%% hratky s jasem 
A_jasny = A+50;
A_tmavy = A-50;
figure;
subplot 131;imshow(A_tmavy);
subplot 132;imshow(A);
subplot 133;imshow(A_jasny);

%% Hratky s kontrastem
A_vicKontrastu = A*2;
A_meneKontrastu = A/2;
figure;
subplot 131;imshow(A_vicKontrastu,[]);
subplot 132;imshow(A);
subplot 133;imshow(A_meneKontrastu,[]);

%% zobrazeni histogramu
imhist(Ag)

%% resize
Ares = imresize(A, [300, 300]);
figure;
subplot 131;imshow(A);
subplot 132;imshow(Ares);
%% translate
translated = imtranslate(A, [15 25]);
imshow(translated);
%% translate affine
outView = imref2d(size(A));
T = [1 0 0; 0 1 0; 15 25 1];
aff = affine2d(T);
cb_translated = imwarp(A, aff);

imshow(cb_translated);
%% flip
A = imread('peppers.png');
%imshow(flip(A,2));
imshow(fliplr(A));

%% affine transform of image
outView = imref2d(size(A));
A = imread('peppers.png');
T = [1 -0.3 0; 1 -0.5, 0; 0, 0, 1];
aff = affine2d(T);
result = imwarp(A, aff);
imshow(result);

%% affine backwards

aff2 = invert(aff);
%%
saturn = imread('saturn.png');
peppers = imread('peppers.png');
szPeppers = size(peppers);

saturn = imresize(saturn, [szPeppers(1), szPeppers(2)]);

result = saturn*0.5 + peppers*0.5;
imshow(result);

%%
result2 = imwarp(result, aff2);
imshow(result2);

%%