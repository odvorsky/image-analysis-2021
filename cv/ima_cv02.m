%% clean all
clc, clear, close all;

%%
img = imread('pout.tif');
imhist(img);
%%
img = imread('pout.tif');
%%imshow(img);
imhist(img);
%%

[counts, bins] = imhist(img);
csum = cumsum(counts);
minC = min(csum);
sz = size(img);

new = round(((csum - minC) / (numel(img) - minC)) * 255);
%%
newHist = histeq(img, new);
imshow(newHist);
%%
fspecial
%%
imhist(newHist);
%%
result2 = histeq(img);
imhist(result2);
%%
adjusted =imadjust(img);
imhist(adjusted);

%%imshow(img);
%%
histeq();
%%
imread('blackrainfrog.jpg');
%%
cam = imread('cameraman.tif');
kernel = [ 
    1 4 6 4 1
    4 10 16 10 4
    6 16 24 16 6
    4 10 16 10 4
    1 4 6 4 1
];
res1 = conv2(cam, kernel);
subplot(121);
imshow(res1, []);

res2 = conv2(res1, fspecial('laplacian'));
subplot(122);
imshow(res2, []);


%% load and show image
RGB = imread('pillsetc.png');
I = rgb2gray(RGB);
bw = imbinarize(I);
%% apply sobel
sobel = fspecial('sobel');
resSobel = imfilter(bw, sobel);
imshow(resSobel);
%% prewitt
prewitt = fspecial('prewitt');
tPrewitt = prewitt';
resPrewitt = conv2(bw, prewitt);
resTPrewitt = conv2(bw, tPrewitt);
figure(1);
subplot 121; imshow(resPrewitt);
subplot 122; imshow(resTPrewitt);

figure(2); imshow(resPrewitt+resTPrewitt);

%% edges...
imshow(edge(bw));

%% 1D FT
%% 2x sinus
Fs = 1000;            % Sampling frequency                    
T = 1/Fs;             % Sampling period       
L = 1500;             % Length of signal
t = (0:L-1)*T;        % Time vector
S1 = 0.7*sin(2*pi*50*t)+ sin(2*pi*100*t); % 50hz s amplitudou 0.7 a 100hz s aplitudou 1
subplot(211)
plot(S1)
Y = fft(S1); % fourierovka
P2 = abs(Y/L); % normalizace aby nam dobre vychazela freq (ka�d� komponenta se vypo��t�va z cel�ho sign�lu)
P1 = P2(1:L/2+1); % ziskani amplitudy - sta�� vz�t jen p�lku hodnot, symetrie podle 0, sta�� n�m jen to kladn�
P1(2:end-1) = 2*P1(2:end-1); % pro korektni interpretaci amplitudy nasobim 2, kvuli symetrii pod�l 0 jsme toti� vyhodili pulku elementu s druhou polovinou hodnoty

f = Fs*(0:(L/2))/L;
subplot(212);
plot(f,P1); 

title('Single-Sided Amplitude Spectrum of S(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
%% vlnity plech na zaklade repmat a jeho frekven�n� spektrum
close all
pocet_bodu = 256;
sin_plech = sin(linspace(1,50,pocet_bodu)); % vytvori sinusovy signal s poctem bodu si, linspace vytvori vektor s hranicemi 1 a 50 a poctem bodu si
A = repmat(sin_plech, pocet_bodu, 1); % vytvori matici, ktera vznikne spojenim sin_plech v pocet_bodu-krat
figure
subplot(1, 2, 1)
imshow(A, [])
title('vlnity plech')
% vypocet spektra
spektrum_obr = fft2(A); % vypocet spektra pomoci rychle fourierovy transformace pro 2D obraz
subplot(1, 2, 2)
spektrum_upraveno= log(fftshift(abs(spektrum_obr))); % posouva nulove frekvence do stredu
imshow(spektrum_upraveno, []) % pokud je�t� zlogaritmujeme, lepe se interpretuj� hodnoty bl�zko 0
title('spektrum')
% vysoke frekvence jsou na krajich, nulova ve stredu
% nizke frekvence reprezentuji nem�nn� ��sti obrazu, naopak ty vysok�
% reprezentuj� kontury, linie, hrany atd.
%% fft na fotce - pillsetc - filtrace n�zk�ch a vysok�ch frekvenc�
image_src = (imread('pillsetc.png'));
im_gr= rgb2gray(image_src);
% vypocet spektra sedotonoveho obrazku
spektrum_obr = fft2(im_gr);
spektrum_obr_uprava = log(fftshift(abs(spektrum_obr)));
subplot(3,3,1)
imshow(im_gr)
title('Puvodni obrazek v sedotonu')
subplot(3,3,2)
imshow(spektrum_obr_uprava, [])
title('spektrum puvodniho')
% filtrace
im_size = size(im_gr); %rozmery obrazu
d = 20; % sirka intervalu pro ctverec binarni masky
maska_dp = zeros(im_size(1), im_size(2)); % tvorba masky
maska_dp((im_size(1)/2)-d:(im_size(1)/2)+d, (im_size(2)/2)-d:(im_size(2)/2)+d) = 1; % vytvoreni masky ze stredu
% filtrace dolni propusti
spektrum_filt_dp = fftshift(spektrum_obr) .* maska_dp; % predpis pro filtraci ve frekvenci oblasti, spektrum je vynasobeno maskou
img_dp = ifft2(spektrum_filt_dp); %zpetna fourierova transformace
% vykresleni vysledku filtrace DP
subplot(3, 3, 4)
imshow((abs(img_dp)), []),title('po DP')
title('Obrazek po DP')
subplot(3,3,5)
imshow(log(abs(spektrum_filt_dp)), [])
title('Spektrum po DP')
subplot(3, 3, 6)
imshow(maska_dp)
title('Maska DP')
% pouziti inverzni masky, horni propust
spektrum_filt_hp = fftshift(spektrum_obr) .* (~maska_dp);
img_hp = ifft2(spektrum_filt_hp); %zpetna fourierova transformace
% vykresleni vysledku filtrace HP
subplot(3, 3, 7)
imshow((abs(img_hp)), []),title('po HP')
title('Obrazek po HP')
subplot(3,3,8)
imshow(log(abs(spektrum_filt_hp)), [])
title('Spektrum po HP')
subplot(3, 3, 9)
imshow(~maska_dp)
title('Maska HP')
%% PCA
clc, clear close all;

S = dir(fullfile('','name*.jpg')); 


